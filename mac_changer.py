# -*- coding: utf-8 -*-
# !/usr/bin/env python

import subprocess
import optparse
import re


def get_arguments():
    parser = optparse.OptionParser()
    #  See parser option when type : name_script.py -h or --help
    parser.add_option("-i", "--interface", dest="interface", help="Interface to change MAC adress")
    parser.add_option("-m", "--mac", dest="new_mac", help="New MAC adress")
    (options, arguments) = parser.parse_args()
    if not options.interface:
        parser.error("[-] Please specify an interface, use --help for more info.")
    elif not options.new_mac:
        parser.error("[-] Please specify a new mac, use --help for more info.")
    return options


def change_mac(interface, new_mac):
    print("[+] Changing MAC address for " + interface + " to " + new_mac)
    subprocess.call(["ifconfig", interface, "down"])
    subprocess.call(["ifconfig", interface, "hw", "ether", new_mac])
    subprocess.call(["ifconfig", interface, "up"])


def get_current_mac(interface):
    # All ifconfig result
    ifconfig_result = subprocess.check_output(["ifconfig", interface])
    #  Regular expression : search(r"regular expression")
    mac_adress_search_result = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", ifconfig_result)
    if mac_adress_search_result:
        return mac_adress_search_result.group(0)
    else:
        print("Could not read MAC adress")


options = get_arguments()

#  Print the current MAC
current_mac = get_current_mac(options.interface)
print("Current MAC = " + str(current_mac))

# change MAC
change_mac(options.interface, options.new_mac)

current_mac = get_current_mac(options.interface)
if current_mac == options.new_mac:
    print("[+] MAC adress was successfuly changed to " + options.new_mac)
else:
    print("[+] MAC adress did not get changed.")
